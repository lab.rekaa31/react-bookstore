import React, { Component } from 'react';
import { connect } from 'react-redux';
import {addFavoriteBook} from './../../store/actions/'

class Card extends Component {
    render() {
        return (
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{this.props.title}</h5>
                    <p class="card-text">{this.props.content}</p>
                    <button onClick={() => this.props.dispatch(addFavoriteBook({id:this.props.id , title:this.props.title}))} class={`btn ${this.props.theme === 1 ? "btn-primary" : "btn-dark"}`}>Add to Favorite</button>
                </div>
            </div>
        );
    }
}

function store(state) {
    return {
        theme : state.status
    }
}

export default connect(store)(Card);
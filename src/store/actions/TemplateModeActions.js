export const CHANGE_THEME = "CHANGE_THEME"

export const changeThemeWeb = ({status}) => ({
    type : CHANGE_THEME,
    payload : {
        status
    }
})
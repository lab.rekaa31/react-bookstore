import { ADD_FAVORITE_BOOK, REMOVE_FAVORITE_BOOK } from './../actions'


export default function booksReducers(state = [], action) {
    switch (action.type) {
        case ADD_FAVORITE_BOOK:
            return [...state, action.payload]
        case REMOVE_FAVORITE_BOOK:
            return state.filter(bookmark => bookmark.id !== action.payload.id)
        default:
            return state
    }
}
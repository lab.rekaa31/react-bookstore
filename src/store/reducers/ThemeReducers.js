import { CHANGE_THEME } from './../actions'


export default function themeWebReducers(state = {status : 0}, action) {
    switch (action.type) {
        case CHANGE_THEME:

            if(state.status === 1){
                return {
                    status : 0
                }
            }

            if(state.status === 0){
                return {
                    status : 1
                }
            }
            // return [...state, action.payload]
        default:
            return state
    }
}